# set terminal postscript eps
# set output "population.eps"
# set title "Population"
set ylabel "%"
set xlabel "iteracja"
# set key top
set logscale y
set nologscale x
plot [][0.01:100] 'population.table' using "%lf%*lf%*lf%*lf" title "P1" with lines, \
                  'population.table' using "%*lf%lf%*lf%*lf" title "P2" with lines, \
                  'population.table' using "%*lf%*lf%lf%*lf" title "P3" with lines, \
                  'population.table' using "%*lf%*lf%*lf%lf" title "P4" with lines
