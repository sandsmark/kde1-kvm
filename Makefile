CC = gcc
CCC = g++
MOC = /usr/lib/qt-1.44/bin/moc
TOUCH = touch
RM = rm
CFLAGS = -DHAVE_PAM -c -O3 -fomit-frame-pointer -Wall \
         -I/usr/lib/qt-1.44/include -I/usr/include/kde
LFLAGS = -L/usr/X11R6/lib/ -L/usr/lib/qt-1.44/lib \
         -lX11 -lXext -lXpm -lICE -lqt
KDELFLAGS = -lkdecore -lkdeui -lcrypt -lpam -ldl -lc

all:	vm vm.kss

vm.c:	vm.h vm_random.h
	$(TOUCH) vm.c
vm.o:	vm.c
	$(CC) $(CFLAGS) vm.c
vm_random.c:	vm_random.h
	$(TOUCH) vm_random.c
vm_random.o:	vm_random.c
	$(CC) $(CFLAGS) vm_random.c
vm_main.moc.cpp: vm_main.h
	$(MOC) vm_main.h -o vm_main.moc.cpp
vm_main.moc.o: vm_main.moc.cpp
	$(CCC) $(CFLAGS) vm_main.moc.cpp
vm_main.cpp: display.h
	$(TOUCH) vm_main.cpp
vm_main.o: vm_main.cpp
	$(CCC) $(CFLAGS) vm_main.cpp
main.moc.cpp: main.h
	$(MOC) main.h -o main.moc.cpp
main.moc.o: main.moc.cpp
	$(CCC) $(CFLAGS) main.moc.cpp
main.cpp: main.h
	$(TOUCH) main.cpp
main.o: main.cpp
	$(CCC) $(CFLAGS) main.cpp
display.moc.cpp: display.h
	$(MOC) display.h -o display.moc.cpp
display.moc.o: display.moc.cpp
	$(CCC) $(CFLAGS) display.moc.cpp
display.cpp: display.h vm.h vm_random.h
	$(TOUCH) display.cpp
display.o: display.cpp
	$(CCC) $(CFLAGS) display.cpp
kvm.moc.cpp: kvm.h
	$(MOC) kvm.h -o kvm.moc.cpp
kvm.moc.o: kvm.moc.cpp
	$(CCC) $(CFLAGS) kvm.moc.cpp
kvm.cpp: vm.h vm_random.h
	$(TOUCH) kvm.cpp
kvm.o: kvm.cpp kvm.moc.cpp
	$(CCC) $(CFLAGS) kvm.cpp
kscreensave.cpp: kscreensave.h
	$(TOUCH) kscreensave.cpp
kscreensave.o: kscreensave.cpp
	$(CCC) $(CFLAGS) kscreensave.cpp
saver.moc.cpp: saver.h
	$(MOC) saver.h -o saver.moc.cpp
saver.moc.o: saver.moc.cpp
	$(CCC) $(CFLAGS) saver.moc.cpp
saver.cpp: saver.h
	$(TOUCH) saver.cpp
saver.o: saver.cpp
	$(CCC) $(CFLAGS) saver.cpp
xlock.cpp: xlock.h
	$(TOUCH) xlock.cpp
xlock.o: xlock.cpp
	$(CCC) $(CFLAGS) xlock.cpp
passwd.o: passwd.cpp
	$(CCC) $(CFLAGS) passwd.cpp
helpers.cpp: helpers.h
	$(TOUCH) helpers.cpp
helpers.o: helpers.cpp
	$(CCC) $(CFLAGS) helpers.cpp
xautolock.cpp: xautolock.h
	$(TOUCH) xautolock.cpp
xautolock.o: xautolock.cpp
	$(CCC) $(CFLAGS) xautolock.cpp
vm:	vm.o display.o display.moc.o vm_main.o vm_random.o
	$(CCC) $(LFLAGS) vm.o display.o display.moc.o vm_main.o vm_random.o \
        -o vm
vm.kss:	vm.o main.o vm_random.o kvm.o kvm.moc.o \
        kscreensave.o saver.o saver.moc.o xlock.o passwd.o helpers.o main.moc.o \
        xautolock.o
	$(CCC) $(LFLAGS) $(KDELFLAGS) vm.o main.o vm_random.o \
        kvm.o kvm.moc.o kscreensave.o saver.o saver.moc.o xlock.o \
        passwd.o helpers.o main.moc.o xautolock.o -o vm.kss

clean:
	$(RM) *.o vm vm.kss
