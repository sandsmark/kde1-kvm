#include "display.h"

#define _COLORS_PRINT

const int program0Length = 4;
int program0[program0Length] = {
 VM_OP_PUSH + 4,
 VM_OP_PUSH + 0,
 VM_OP_PUSH - 4,
 VM_OP_COPY,
};
int rules0[program0Length] = {
 '+',
 'e',
 'e',
 'e',
};

const int program1Length = 6;
int program1[program1Length] = {
 VM_OP_PUSH + 6,
 VM_OP_PUSH + 2,
 VM_OP_PUSH - 4,
 VM_OP_COPY,
 VM_OP_PUSH - 6,
 VM_OP_EXEC,
};
int rules1[program1Length] = {
 '+',
 'e',
 'e',
 'e',
 'e',
 'e',
};

const int program2Length = 8;
int program2[program2Length] = {
 VM_OP_PUSH + 9,
 VM_OP_PUSH + 4,
 VM_OP_PUSH - 4,
 VM_OP_COPY,
 VM_OP_PUSH + 9,
 VM_OP_PUSH + 4,
 VM_OP_PUSH - 4,
 VM_OP_EXEC,
};
int rules2[program2Length] = {
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
};

const int program3Length = 12;
int program3[program3Length] = {
 VM_OP_PUSH + 9,
 VM_OP_PUSH + 4,
 VM_OP_PUSH - 4,
 VM_OP_EXEC,
 VM_OP_EXEC,
 VM_OP_PUSH + 4,
 VM_OP_PUSH - 4,
 VM_OP_COPY,
 VM_OP_PUSH + 9,
 VM_OP_PUSH + 4,
 VM_OP_PUSH - 4,
 VM_OP_EXEC,
};
int rules3[program3Length] = {
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
 'e',
};

#ifndef COLORS_PRINT
QColor* vmGetOpColor( struct tvm_pool* pool, int index ) {
 QColor* color = new QColor;
 if( index >= pool->area_size )
  color->setRgb( 255, 255, 255 );
 else
  switch( pool->area[index] ) {
   case VM_OP_STOP:
    color->setRgb( 0, 0, 0 );
    break;

   case VM_OP_EXEC:
    color->setRgb( 255, 0, 0 );
    break;

   case VM_OP_COPY:
    color->setRgb( 192, 192, 0 );
    break;

   case VM_OP_WAIT:
    color->setRgb( 240, 240, 240 );
    break;

   default:
    int intensity = 155 - 10*(pool->area[index] - VM_OP_PUSH);
    if( intensity < 0 )
     intensity = 0;
    if( intensity > 255 )
     intensity = 255;
    color->setRgb( 0, 128, intensity );
    break;

  }
 return color;
}
#else
QColor* vmGetOpColor( struct tvm_pool* pool, int index ) {
 QColor* color = new QColor;
 color->setRgb( 255, 255, 255 );
 return color;
}
#endif

VmZoom::VmZoom( struct tvm_pool* pool, char* modified,
                const int columns, const int rows,
                const int displayColumns, const int displayRows,
                QWidget* parent, const char* name )
 : QWidget( parent, name ) {
 this->pool = pool;
 this->modified = modified;
 this->columns = columns;
 this->rows = rows;
 this->displayColumns = displayColumns;
 this->displayRows = displayRows;
 prevBaseColumn = 0;
 prevBaseRow = 0;
 baseColumn = 0;
 baseRow = 0;
 cellWidth = 24;
 cellHeight = 16;
 resize( displayColumns*cellWidth + 1, displayRows*cellHeight + 1 );
 setBackgroundMode( NoBackground );
 setCaption( "VM area zoom" );
}
VmZoom::~VmZoom() {
}
void VmZoom::setBase( const int baseColumn, const int baseRow ) {
 this->baseColumn = baseColumn;
 this->baseRow = baseRow;
 if( this->baseColumn < 0 )
  this->baseColumn = 0;
 if( this->baseColumn > columns - displayColumns )
  this->baseColumn = columns - displayColumns;
 if( this->baseRow < 0 )
  this->baseRow = 0;
 if( this->baseRow > rows - displayRows )
  this->baseRow = rows - displayRows;
 repaint();
}
void VmZoom::clearModified() {
 for( int y = 0; y < displayRows; ++y )
  for( int x = 0; x < displayColumns; ++x )
   modified[( y + baseRow )*columns + ( x + baseColumn )] = 0;
}
void VmZoom::update( bool redrawAll ) {
 redrawAll = redrawAll || prevBaseColumn != baseColumn ||
                          prevBaseRow != baseRow;
 QPainter painter( this );
 for( int y = 0; y < displayRows; ++y )
  for( int x = 0; x < displayColumns; ++x ) {
   int index = ( y + baseRow )*columns + ( x + baseColumn );
   if( modified[index] || redrawAll ) {
    int baseX = x*cellWidth;
    int baseY = y*cellHeight;
    QString str;
    int num;
    if( index >= pool->area_size )
     num = 0;
    else
     num = pool->area[index];
    if( num == VM_OP_COPY ) 
     str.append( "C" );
    else if( num == VM_OP_EXEC ) 
     str.append( "X" );
    else if( num == VM_OP_STOP ) 
     str.append( "S" );
    else {
     str.setNum( num - VM_OP_PUSH );
    }
#ifdef QT2
    painter.setPen( Qt::black );
#else
    painter.setPen( black );
#endif
    QColor* color = vmGetOpColor( pool, index );
    painter.setBrush( *color );
    painter.drawRect( baseX, baseY, cellWidth + 1, cellHeight + 1 );
#ifdef QT2
#ifndef COLORS_PRINT
    painter.setPen( Qt::white );
#else
    painter.setPen( Qt::black );
#endif
#else
#ifndef COLORS_PRINT
    painter.setPen( white );
#else
    painter.setPen( black );
#endif
#endif
    painter.drawText( baseX + 2, baseY + cellHeight - 3, str );
   }
  }
 prevBaseColumn = baseColumn;
 prevBaseRow = baseRow;
}
void VmZoom::paintEvent( QPaintEvent* ) {
 update( 1 );
}


VmStatus::VmStatus( QWidget* parent, const char* name )
 : QWidget( parent, name ) {
 QVBoxLayout* topLayout = new QVBoxLayout( this, 4 );
 labelIteration = new QLabel( this, "iteration" );
 labelIteration->setMinimumSize( 128, 32 );
 topLayout->addWidget( labelIteration, 10 );
 this->setCaption( "Vm Status" );
 this->resize( 120, 24 );
}
VmStatus::~VmStatus() {
 
}
void VmStatus::setIteration( const int iteration ) {
 QString	str;

 this->iteration = iteration;
 str.setNum( this->iteration );
 str.prepend( "C = " );
 labelIteration->setText( str );
}

VmDisplay::VmDisplay( struct tvm_pool* pool, const int columns,
                      const int size, const int displayStep,
                      QWidget* parent, const char* name )
 : QWidget( parent, name ) {
 this->pool = pool;
 this->columns = columns;
 this->size = size;
 this->displayStep = displayStep;
 rows = ( pool->area_size + this->columns - 1 )/this->columns;
 resize( this->columns*this->size + 1,
         this->rows*this->size + 1 );
 setBackgroundMode( NoBackground );
 setCaption( "VM area" );
 modified = new char[this->rows*this->columns];
 for( int x = 0; x < this->rows*this->columns; ++x )
  modified[x] = 1;
 zoomDisplayColumns = 24;
 zoomDisplayRows = 16;
 zoom = new VmZoom( pool, modified,
                    this->columns, this->rows,
                    zoomDisplayColumns, zoomDisplayRows );
 zoom->show();
 iteration = 0;
 status = new VmStatus();
 status->show();
 QTimer *timer = new QTimer( this );
 connect( timer, SIGNAL(timeout()), SLOT(iterate()) );
 timer->start( 0, FALSE );
 vm_default_initstate( 1000, &(pool->vm_random_data) );
}
VmDisplay::~VmDisplay() {
 delete[] modified;
 delete zoom;
}
void VmDisplay::update( bool redrawAll ) {
 QPainter painter( this );
 for( int y = 0; y < rows; ++y )
  for( int x = 0; x < columns; ++x ) {
   int position;
   position = y*columns + x;
   if( redrawAll || modified[position] ) {
    QColor* color = vmGetOpColor( pool, position );
#ifdef QT2
    painter.setPen( Qt::black );
#else
    painter.setPen( black );
#endif
    painter.setBrush( *color );
    painter.drawRect( x*size, y*size, size + 1, size + 1);
    delete color;
    modified[position] = 0;
   }
  }
}
void VmDisplay::paintEvent( QPaintEvent* event ) {
 update( 1 );
}
int VmDisplay::getRandom( const int max_value ) {
 return (int)( vm_random(&(pool->vm_random_data))*1.0*(max_value + 1.0)/
               (VM_RAND_MAX + 1.0) );
}
void VmDisplay::modifyArea( const int op ) {
 int position;

 vm_modify( pool, position = getRandom(pool->area_size - 1), op );
 modified[position] = 1;
}
void VmDisplay::loadProgramm( int* const pattern, const int patternLength,
                             const int beginPosition, const int execPosition ) {
 for( int position = 0; position < patternLength; ++position )
  vm_modify( pool, beginPosition + position, pattern[position] );
 vm_exec( pool, execPosition, 0, vm_get_reverse(pool) );
}
int VmDisplay::findPatternCount( int* const pattern, int* const rules, const int patternLength ) {
 int count = 0;
 for( int j = 0; j < pool->area_size + 1 - patternLength; ++j )
  for( int i = 0; i < patternLength; ++i ) {
   bool condition;
   if( rules[i] == 'e' )
    condition = pool->area[j + i] == pattern[i];
   else if( rules[i] == '+' )
    condition = pool->area[j + i] >= pattern[i];
   else if( rules[i] == '-' )
    condition = pool->area[j + i] <= pattern[i];
   if( !condition )
    break;
   else if( i == patternLength - 1 )
    ++count;
  }
 return count;
}
void VmDisplay::iterate() {
 for( int i = 0; i < 1; ++i ) {
  if( getRandom(0) == 0 )
   modifyArea( VM_OP_PUSH + getRandom(10) - getRandom(10) );
  if( getRandom(2) == 0 )
   modifyArea( VM_OP_STOP );
  if( getRandom(2) == 0 )
   modifyArea( VM_OP_COPY );
  if( getRandom(2) == 0 )
   modifyArea( VM_OP_EXEC );
 }
 if( getRandom(0) == 0 )
  vm_exec( pool, getRandom(this->pool->area_size - 1), 0,
           vm_get_reverse(pool) );
 vm_iterate( pool, modified );
 fprintf( stdout, "%7.3f %7.3f %7.3f %7.3f\n", 
          findPatternCount(program0, rules0, program0Length)*program0Length/100.0,
          findPatternCount(program1, rules1, program1Length)*program1Length/100.0,
          findPatternCount(program2, rules2, program2Length)*program2Length/100.0,
          findPatternCount(program3, rules3, program3Length)*program3Length/100.0 );
 if( iteration++%displayStep == 0 ) {
  zoom->update( 0 );
  if( !isVisible() )
   zoom->clearModified();
  if( iteration%57 == 0 )
   status->setIteration( iteration );
  update( 0 );
 }
}
void VmDisplay::mousePressEvent ( QMouseEvent* event ) {
 QPoint point = event->pos();
 zoom->setBase( point.x()/size - zoomDisplayColumns/2, point.y()/size - zoomDisplayRows/2 );
}
void VmDisplay::mouseMoveEvent ( QMouseEvent* event ) {
 QPoint point = event->pos();
 zoom->setBase( point.x()/size - zoomDisplayColumns/2, point.y()/size - zoomDisplayRows/2 );
}
