#include <qapp.h>
extern "C" {
 #include "vm.h"
} 
#include "display.h"

#define	POOL_AREA_SIZE		10000
#define	THREAD_MAX_STACK_SIZE	10
#define	MAX_THREADS_NUM		1000

int main( int argc, char** argv ) {
 struct tvm_pool*	pool;
 
 if( vm_init_pool(&pool, POOL_AREA_SIZE, THREAD_MAX_STACK_SIZE, 
                  MAX_THREADS_NUM) ) {
  QApplication a( argc, argv );
  VmDisplay* vmDisplay = new VmDisplay( pool, 100, 5, 1 );
  a.setMainWidget( vmDisplay );
  vmDisplay->show();
  a.exec();
  vm_done_pool( pool );
 }
 return 0;
}
