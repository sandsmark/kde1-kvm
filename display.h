#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <stdlib.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qtimer.h>
extern "C" {
 #include "vm.h"
 #include "vm_random.h"
}

class VmZoom : public QWidget {
 Q_OBJECT
 
 struct tvm_pool* pool;
 char* modified;
 int columns;
 int rows;
 int displayColumns;
 int displayRows;
 int prevBaseColumn;
 int prevBaseRow;
 int baseColumn;
 int baseRow;
 int* values;
 int cellWidth;
 int cellHeight;

 public:
  VmZoom::VmZoom( struct tvm_pool* pool, char* modified,
                  const int columns, const int rows,
                  const int displayColumns, const int displayRows,
                  QWidget* parent = 0, const char* name = 0 );
  VmZoom::~VmZoom();
  void setBase( const int baseColumn, const int baseRow );
  void clearModified();
  void update( bool redrawAll );

 private:
  void paintEvent( QPaintEvent* );
};

class VmStatus : public QWidget {
 Q_OBJECT

 int		iteration;
 QLabel*	labelIteration;

 public:
  VmStatus::VmStatus( QWidget* parent = 0, const char* name = 0 );
  VmStatus::~VmStatus();

 public slots:
  void setIteration( const int iteration );
};

class VmDisplay : public QWidget {
 Q_OBJECT
 
 struct tvm_pool* pool;
 int columns;
 int rows;
 int size;
 int displayStep;
 int iteration;
 char* modified;
 VmZoom* zoom;
 int zoomDisplayColumns;
 int zoomDisplayRows;
 VmStatus* status;
 
 public:
  VmDisplay::VmDisplay( struct tvm_pool* pool, const int columns,
                        const int size, const int displayStep,
                        QWidget* parent = 0, const char* name = 0 );
  VmDisplay::~VmDisplay();
  void update( bool redrawAll );
  
 protected:
  int getRandom( const int max_value );
  void modifyArea( const int op );
  void loadProgramm( int* const pattern, const int patternLength,
                     const int beginPosition, const int execPosition );
  int findPatternCount( int* const pattern, int* const rules, const int patternLength );
 
 protected slots:
  void iterate();
 
 private:
  void paintEvent( QPaintEvent* event );
  void mousePressEvent ( QMouseEvent* event );
  void mouseMoveEvent ( QMouseEvent* event );
};

#endif /* !defined( __DISPLAY_H__ ) */
